package com.junior.stronger197.currencyexchanger.activities.base.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.google.gson.Gson
import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.R.id.*
import com.junior.stronger197.currencyexchanger.activities.base.viewmodel.BaseViewModel
import com.junior.stronger197.currencyexchanger.fragments.exchange.view.ExchangeFragment
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.view.ExchangeListFragment
import com.junior.stronger197.currencyexchanger.fragments.filters.view.FiltersFragment
import com.junior.stronger197.currencyexchanger.fragments.history.view.HistoryFragment
import kotlinx.android.synthetic.main.activity_base.*


lateinit var baseViewModel: BaseViewModel

class BaseActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            navigation_exchange -> {
                showExchangeFragment()
                return@OnNavigationItemSelectedListener true
            }
            navigation_history -> {
                showHistoryFragment()
                return@OnNavigationItemSelectedListener true
            }
            navigation_analytics -> {
                showAnalyticsFragment()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    fun showFiltersFragment() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.framelayout_base_fragmentcontainer, FiltersFragment())
                .addToBackStack(null)
                .commit()
    }

    fun showExchangeFragment(primaryCurrency: Currency?, secondaryCurrency: Currency?) {
        var gson = Gson()
        val exchangeFragment = ExchangeFragment()
        val args = Bundle()
        args.putString("primaryCurrency", gson.toJson(primaryCurrency))
        args.putString("secondaryCurrency", gson.toJson(secondaryCurrency))
        exchangeFragment.arguments = args

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.framelayout_base_fragmentcontainer, exchangeFragment)
                .addToBackStack(null)
                .commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        baseViewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)

        bottom_navigation_view.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun showExchangeFragment() {
        changeFragment(ExchangeListFragment())
    }

    private fun showHistoryFragment() {
        changeFragment(HistoryFragment())
    }

    private fun showAnalyticsFragment() {
        // TODO logic to show analytics fragment
    }

    private fun changeFragment(fragment : Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.framelayout_base_fragmentcontainer, fragment)
                .commit()
    }
}
