package com.junior.stronger197.currencyexchanger.fragments.exchangelist.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "currencies")
data class Currency(
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "currency_name")
        var name: String = "",

        @ColumnInfo(name = "currency_rate")
        var rate: Float? = null,

        @ColumnInfo(name = "currency_last_update")
        var lastUpdate: Long? = 0,

        @ColumnInfo(name = "currency_last_usage")
        var lastUsage: Long? = 0,

        @ColumnInfo(name = "currency_favorite")
        var isFavorite: Boolean = false
)

@Entity(tableName = "history")
data class History(
        @ColumnInfo(name = "history_first_currency")
        var firstCurrency: String? = null,

        @ColumnInfo(name = "history_secondary_currency")
        var secondaryCurrency: String? = null,

        @ColumnInfo(name = "history_first_value")
        var firstValue: Double? = null,

        @ColumnInfo(name = "history_secondary_value")
        var secondaryValue: Double? = null,

        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "history_date")
        var date: Long
)