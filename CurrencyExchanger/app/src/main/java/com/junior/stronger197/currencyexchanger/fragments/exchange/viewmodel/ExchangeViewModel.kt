package com.junior.stronger197.currencyexchanger.fragments.exchange.viewmodel


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.junior.stronger197.currencyexchanger.fragments.exchange.model.CurrenciesRepository
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.History
import com.junior.stronger197.currencyexchanger.helpers.database.DatabaseHelper
import com.junior.stronger197.currencyexchanger.helpers.network.RetrofitHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ExchangeViewModel : ViewModel() {

    var firstValue = 0.0
    var secondValue = 0.0

    var firstCurrencyTextView = MutableLiveData<String>()
    var secondaryCurrencyTextView = MutableLiveData<String>()

    var firstCurrencyEditText = MutableLiveData<Double>()
    var secondaryCurrencyEditText = MutableLiveData<Double>()

    var exchangeButtonEnabled = MutableLiveData<Boolean>()

    private var firstCurrency = Currency()
    private var secondaryCurrency = Currency()

    init {
        observe()
    }

    fun onArgumentReceived(firstCurrency: Currency?, secondaryCurrency: Currency?) {
        if (firstCurrency != null) {
            this.firstCurrency = firstCurrency
            this.firstCurrencyTextView.postValue(firstCurrency.name)
        }

        if (secondaryCurrency != null) {
            this.secondaryCurrency = secondaryCurrency
            this.secondaryCurrencyTextView.postValue(secondaryCurrency.name)
        }

        firstCurrencyEditText.value = 1.0
        secondaryCurrencyEditText.value = convert(1.0, firstCurrency!!, secondaryCurrency!!)
    }

    fun onViewCreated() {
        exchangeButtonEnabled.value = !(System.currentTimeMillis() / 1000 - firstCurrency.lastUpdate!! > 300
                || System.currentTimeMillis() / 1000 - secondaryCurrency.lastUpdate!! > 300)

        observe()
    }

    fun firstCurrencyChanged(value : Double) {
        Log.e("1", "First currency changed")

        CurrenciesRepository.setFirstCurrencyValue(value)
        requestNewExchangeRate()
    }

    fun changeFirstCurrency(value: Double) {
        secondValue = convert(value, firstCurrency, secondaryCurrency)
        firstValue = value
        secondaryCurrencyEditText.postValue(secondValue)
    }

    fun changeSecondaryCurrency(value: Double) {
        firstValue = convert(value, secondaryCurrency, firstCurrency)
        secondValue = value
        firstCurrencyEditText.postValue(firstValue)
    }

    fun secondCurrencyChanged(value: Double) {
        Log.e("1", "Sec currency changed")

        CurrenciesRepository.setSecondCurrencyValue(value)
        requestNewExchangeRate()
    }

    private fun convertFrom(value: Double, fromRate: Double): Double {
        return value / fromRate
    }

    private fun convertTo(value: Double, toRate: Double): Double {
        return value * toRate
    }

    private fun requestNewExchangeRate() {
        Log.e("TEST NETWORK", "REQUEST INVOKE")
//        RetrofitHelper.getFixerApi().getLatestRates(arrayOf(firstCurrency!!.name + " " + secondaryCurrency!!.name)).map { t ->
//            val data = ArrayList<Currency>()
//            Log.e("TEST NETWORK", "RESPONSE0: $t")
//            var date = parseDate(t.date!!)
//            t.rates?.forEach { data.add(Currency(it.key, it.value, date)) }
//            data
//        }
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    Log.e("TEST NETWORK", "RESPONSE")
//                    Log.e("TEST NETWORK", "RESPONSE: $it")
//                    if(it.first().name == firstCurrency!!.name) {
//                        firstCurrency!!.rate = it.first().rate
//                        firstCurrency!!.lastUpdate = it.first().lastUpdate
//
//                        secondaryCurrency!!.rate = it[1].rate
//                        secondaryCurrency!!.lastUpdate = it[1].lastUpdate
//                    } else {
//                        firstCurrency!!.rate = it[1].rate
//                        firstCurrency!!.lastUpdate = it[1].lastUpdate
//
//                        secondaryCurrency!!.rate = it.first().rate
//                        secondaryCurrency!!.lastUpdate = it.first().lastUpdate
//                    }
//
//                    DatabaseHelper.db.currencyDao.update(firstCurrency.name, firstCurrency.rate!!, firstCurrency.lastUpdate!!)
//                    DatabaseHelper.db.currencyDao.update(secondaryCurrency.name, secondaryCurrency!!.rate!!, secondaryCurrency.lastUpdate!!)
//
//                }, {
//                    Log.e("TEST NETWORK", "ERROR", it)
//                })

        RetrofitHelper.getFixerApi().getLatestRates()
                .subscribeOn(Schedulers.io()).map { t ->
            var lastUpdate = System.currentTimeMillis() / 1000
            val data = ArrayList<Currency>()
            t.rates?.forEach { data.add(Currency(it.key, it.value, lastUpdate)) }
            data
        }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.isNotEmpty()) {
                        Log.e("TEST NETWORK", "RESOINSE: " + it)
                        Thread(Runnable {
                            it.forEach {
                                DatabaseHelper.db.currencyDao.insert(it)
                                if(it.name == firstCurrency.name) {
                                    firstCurrency = it
                                }

                                if(it.name == secondaryCurrency.name) {
                                    secondaryCurrency = it
                                }
                            }

                            updateUI()
                        }).start()
                    }
                }, {
                    Log.e("TEST NETWORK", "ERROR", it)
                })


    }

    private fun observe() {
        CurrenciesRepository.firstCurrencyValue.observeOn(AndroidSchedulers.mainThread()).subscribe({
           changeFirstCurrency(it)
        })

        CurrenciesRepository.secondCurrencyValue.observeOn(AndroidSchedulers.mainThread()).subscribe({
            changeSecondaryCurrency(it)
        })

        observeRate()
    }

    private fun updateUI() {
        Log.e("UPDATEUI", "INVOKE()")
        Log.e("UPDATEUI", "INVOKE() sys: " + System.currentTimeMillis() / 1000)
        Log.e("UPDATEUI", "INVOKE() first: " + firstCurrency.lastUpdate!!)
        Log.e("UPDATEUI", "INVOKE() sec: " + secondaryCurrency.lastUpdate!!)

        if(System.currentTimeMillis() / 1000 - firstCurrency.lastUpdate!! <= 15
            && System.currentTimeMillis() / 1000 - secondaryCurrency.lastUpdate!! <= 15) {
            exchangeButtonEnabled.postValue(true)
        } else {
            exchangeButtonEnabled.postValue(false)
        }
    }

    private fun observeRate() {
        DatabaseHelper.db.currencyDao.getAllAsync()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.e("TEST NETWORK", "RATE CHANGE" + it)
                    updateUI()
                })

        DatabaseHelper.db.currencyDao.getAsync(secondaryCurrency.name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.e("TEST NETWORK", "RATE CHANGE" + it)
                    updateUI()
                })
    }

    private fun convert(value : Double, firstCurrency: Currency, secondaryCurrency: Currency) : Double {
        var fromRate = firstCurrency.rate
        var toRate = secondaryCurrency.rate
        val valueInEUR = convertFrom(value, fromRate!!.toDouble())


        return convertTo(valueInEUR, toRate!!.toDouble())
    }

    fun buttonClicked(firstCurrency: Currency, secondaryCurrency: Currency, valueFirst: Double, valueSecondary : Double) {
        Thread({
            DatabaseHelper.db.historyDao.insert(History(firstCurrency.name, secondaryCurrency.name, valueFirst, valueSecondary, System.currentTimeMillis() / 1000))
            Log.e("CURRENT_HISTORY", DatabaseHelper.db.historyDao.getAll().toString())
        }).start()


    }
}