package com.junior.stronger197.currencyexchanger.fragments.filters.view


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.fragments.filters.model.DateRange
import com.junior.stronger197.currencyexchanger.fragments.filters.model.FiltersRepository
import kotlinx.android.synthetic.main.fragment_filters.*
import java.util.*
import android.app.DatePickerDialog
import java.text.SimpleDateFormat


class FiltersFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_filters, container, false)
    }

    var fromDate = Calendar.getInstance()
    var toDate = Calendar.getInstance()

    var range = DateRange(0, System.currentTimeMillis() / 1000)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 10);

        var lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -10);

        radio_all.isChecked = true

        radio_group.setOnCheckedChangeListener({ group, checkedId ->
            if (radio_all.isChecked()) {
                from_date_edit_text.visibility = View.GONE
                to_date_edit_text.visibility = View.GONE
                var to = System.currentTimeMillis() / 1000
                var from = 0L
                range = DateRange(from, null)

               // ALL
            } else if (radio_week.isChecked()) {
               // ONE WEEK RANGE
                from_date_edit_text.visibility = View.GONE
                to_date_edit_text.visibility = View.GONE
                var to = System.currentTimeMillis() / 1000
//                var from = System.currentTimeMillis() / 1000 - 604800
                var from = System.currentTimeMillis() / 1000 - 30
                range = DateRange(from, null)

            } else if(radio_mounth.isChecked) {
                from_date_edit_text.visibility = View.GONE
                to_date_edit_text.visibility = View.GONE
                var to = System.currentTimeMillis() / 1000
                var from = System.currentTimeMillis() / 1000 - 2592000
                range = DateRange(from, null)

            } else {

                from_date_edit_text.visibility = View.VISIBLE
                to_date_edit_text.visibility = View.VISIBLE

                from_date_edit_text.setOnClickListener {
                    DatePickerDialog(activity, d1,
                            fromDate.get(Calendar.YEAR),
                            fromDate.get(Calendar.MONTH),
                            fromDate.get(Calendar.DAY_OF_MONTH))
                            .show()
                }

                to_date_edit_text.setOnClickListener {
                    DatePickerDialog(activity, d2,
                            toDate.get(Calendar.YEAR),
                            toDate.get(Calendar.MONTH),
                            toDate.get(Calendar.DAY_OF_MONTH))
                            .show()
                }

            }
        })


        apply_button.setOnClickListener {
            if(radio_selecttime.isChecked) {
                range = DateRange(fromDate.timeInMillis / 1000, toDate.timeInMillis / 1000)
            }
            FiltersRepository.setDate(range)
            fragmentManager!!.popBackStack()
        }
    }



    val d1 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        fromDate.set(Calendar.YEAR, year)
        fromDate.set(Calendar.MONTH, monthOfYear)
        fromDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        from_date_edit_text.setText(SimpleDateFormat("dd-MM-yyyy").format(fromDate.time))
    }

    val d2 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        toDate.set(Calendar.YEAR, year)
        toDate.set(Calendar.MONTH, monthOfYear)
        toDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)


        to_date_edit_text.setText(SimpleDateFormat("dd-MM-yyyy").format(toDate.time))
    }
}
