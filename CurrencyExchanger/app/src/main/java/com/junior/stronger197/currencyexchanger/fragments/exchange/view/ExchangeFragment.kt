package com.junior.stronger197.currencyexchanger.fragments.exchange.view


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson

import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.fragments.exchange.viewmodel.ExchangeViewModel
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import kotlinx.android.synthetic.main.fragment_exchange.*
import android.view.View.OnFocusChangeListener


class ExchangeFragment : Fragment() {

    private lateinit var exchangeViewModel: ExchangeViewModel

    private var firstCurrency: Currency? = null
    private var secondCurrency: Currency? = null

    override fun onAttach(context: Context?) {
        exchangeViewModel = ViewModelProviders.of(this).get(ExchangeViewModel::class.java)

        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val gson = Gson()
        if (arguments != null) {
            var firstCurrencyJson = arguments!!.getString("primaryCurrency", null)
            var secondCurrencyJson = arguments!!.getString("secondaryCurrency", null)


            if (firstCurrencyJson != null) {
                firstCurrency = gson.fromJson<Currency>(firstCurrencyJson, Currency::class.java)
            }

            if (secondCurrencyJson != null) {
                secondCurrency = gson.fromJson(secondCurrencyJson, Currency::class.java)
            }

            exchangeViewModel.onArgumentReceived(firstCurrency, secondCurrency)
        }


        observe()
        return inflater.inflate(R.layout.fragment_exchange, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        exchangeViewModel.onViewCreated()

        edittext_exchange_firstvalue.onFocusChangeListener = OnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                edittext_exchange_firstvalue.addTextChangedListener(firstValueTextChangeListener)
            } else {
                edittext_exchange_firstvalue.removeTextChangedListener(firstValueTextChangeListener)
            }
        }

        edittext_exchange_secondaryvalue.onFocusChangeListener = OnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                edittext_exchange_secondaryvalue.addTextChangedListener(secondaryValueTextChangeListener)
            } else {
                edittext_exchange_secondaryvalue.removeTextChangedListener(secondaryValueTextChangeListener)
            }
        }


        exchange_button.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                exchangeViewModel.buttonClicked(firstCurrency!!, secondCurrency!!, edittext_exchange_firstvalue.text.toString().toDouble(), edittext_exchange_secondaryvalue.text.toString().toDouble())
            }
        })
    }

    private var firstValueTextChangeListener: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            // ...
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            // ...
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            try {
                exchangeViewModel.firstCurrencyChanged(p0.toString().replace("\\D+", "").toDouble())
            } catch (e: Exception) {
                //
            }
        }

    }

    private var secondaryValueTextChangeListener: TextWatcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            // ...
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            // ...
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            try {
                exchangeViewModel.secondCurrencyChanged(p0.toString().replace("\\D+", "").toDouble())
            } catch (e: Exception) {
                //
            }
        }
    }

    /*

     */

    private fun observe() {

        exchangeViewModel.firstCurrencyTextView.observe(this, Observer<String> {
            textview_exchange_firstcurrency.text = it
        })

        exchangeViewModel.secondaryCurrencyTextView.observe(this, Observer<String> {
            textview_exchange_secondarycurrency.text = it
        })

        exchangeViewModel.firstCurrencyEditText.observe(this, Observer<Double> {
            edittext_exchange_firstvalue.removeTextChangedListener(firstValueTextChangeListener)
            edittext_exchange_firstvalue.setText(it.toString())
            edittext_exchange_firstvalue.addTextChangedListener(firstValueTextChangeListener)
        })

        exchangeViewModel.secondaryCurrencyEditText.observe(this, Observer<Double> {
            edittext_exchange_secondaryvalue.removeTextChangedListener(secondaryValueTextChangeListener)
            edittext_exchange_secondaryvalue.setText(it.toString())
            edittext_exchange_secondaryvalue.addTextChangedListener(secondaryValueTextChangeListener)
        })

        exchangeViewModel.exchangeButtonEnabled.observe(this, Observer<Boolean> {
            exchange_button.isEnabled = it!!
        })

    }
}
