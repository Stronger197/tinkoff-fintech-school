package com.junior.stronger197.currencyexchanger.fragments.exchangelist.view

import android.graphics.Color
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import com.junior.stronger197.currencyexchanger.helpers.getCountryFlagFromLocale
import java.util.*
import kotlin.collections.ArrayList

class CurrenciesListAdapter(listOfCurrencies: ArrayList<Currency>, onCurrencyClick: OnCurrencyClick) : RecyclerView.Adapter<CurrenciesListAdapter.ViewHolder>() {

    private var dataset: ArrayList<Currency> = listOfCurrencies
    private var onClickListener: OnCurrencyClick = onCurrencyClick

    private var selectedItem: Currency? = null

    private enum class Types(val type: Int) {
        DEFAULT(1),
        FAVORITE(2),
        SELECTED(3)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.currency_recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataset[position].isFavorite) {
            Types.FAVORITE.type
        } else if (selectedItem == dataset[position]) {
            Types.SELECTED.type
        } else {
            Types.DEFAULT.type
        }
    }

    fun makeItemSelected(item: Currency) {
        selectedItem = item

        var index = dataset.indexOf(item)
        notifyItemChanged(index)
        updateDataset(dataset)
    }

    fun removeItemSelection(item: Currency) {
        selectedItem = null

        var index = dataset.indexOf(item)
        notifyItemChanged(index)
        updateDataset(dataset)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val type = getItemViewType(position)

        val text = getCountryFlagFromLocale(dataset[position].name!!) + dataset[position].name!!
        holder.nameTextView.text = text
        holder.itemView.setOnClickListener {
            Log.d("CLICKLISTENER", "ON CLICK()")
            var clickedItem = dataset[holder.adapterPosition]
            Log.d("CLICKLISTENER", "Clicked item: " + clickedItem)
            if (selectedItem != null) {
                Log.d("CLICKLISTENER", "selected not null. is: " + selectedItem)
                onClickListener.onClick(clickedItem, selectedItem)
            } else {
                Log.d("CLICKLISTENER", "selectedISNULL")
                var flag = false
                var EURIndex = -1
                var USDIndex = -1

                dataset.forEachIndexed { index, currency ->
                    if (currency != clickedItem) {
                        if (currency.isFavorite) {
                            if(!flag) {
                            onClickListener.onClick(clickedItem, currency)
                            flag = true
                            }

                        }

                        if (currency.name == "EUR") {
                            EURIndex = index
                        }

                        if (currency.name == "USD") {
                            USDIndex = index
                        }
                    }
                }

                if (flag == false) {
                    if (clickedItem.name != "EUR") {
                        onClickListener.onClick(clickedItem, dataset[EURIndex])
                    } else {
                        onClickListener.onClick(clickedItem, dataset[USDIndex])
                    }
                }
            }
        }
        holder.itemView.setOnLongClickListener {
            var selected = dataset[holder.adapterPosition]
            if (selected != selectedItem) {
                makeItemSelected(selected)
            } else {
                removeItemSelection(selected)
            }

            onClickListener.onLongClick(dataset[holder.adapterPosition])
            true
        }
        holder.starImageButton.setOnClickListener {
            onClickListener.onStarClick(dataset[holder.adapterPosition])
        }
        if (type == Types.FAVORITE.type) {
            holder.starImageButton.setImageResource(R.drawable.ic_star)
        }
        if (type == Types.SELECTED.type) {
            holder.itemView.setBackgroundColor(Color.BLUE)
        }
    }

    fun updateDataset(newDataset: ArrayList<Currency>) {

        val diffCallback = CurrenciesDiffCallback(dataset, newDataset)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        dataset = newDataset
        diffResult.dispatchUpdatesTo(this)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTextView: TextView = itemView.findViewById(R.id.textview_recylcerview_currencyname)
        var starImageButton: ImageButton = itemView.findViewById(R.id.imagebutton_recyclerview_star)
    }

    fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(dataset, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(dataset, i, i - 1)
            }
        }

        notifyItemMoved(fromPosition, toPosition)
        return true
    }

}


class CurrenciesDiffCallback(oldCurrenciesList: ArrayList<Currency>, newCurrenciesList: ArrayList<Currency>) : DiffUtil.Callback() {

    private var oldList = oldCurrenciesList
    private var newList = newCurrenciesList

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].name == newList[newItemPosition].name
    }

    init {
        Log.e("DIFFUTIL", "old item list: $oldList")
        Log.e("DIFFUTIL", "new item list $newList")
    }


    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {

        var same = oldList[oldItemPosition].name == newList[newItemPosition].name
                && oldList[oldItemPosition].rate == newList[newItemPosition].rate
                && oldList[oldItemPosition].lastUpdate == newList[newItemPosition].lastUpdate
                && oldList[oldItemPosition].isFavorite == newList[newItemPosition].isFavorite

        return same
    }
}

class SimpleItemTouchHelperCallback(private val adapter: CurrenciesListAdapter) : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        adapter.onItemMove(viewHolder.adapterPosition, target.adapterPosition)

        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        // TODO something
    }

}

interface OnCurrencyClick {
    fun onClick(currency: Currency, secondary: Currency?)
    fun onLongClick(currency: Currency)
    fun onStarClick(currency: Currency)
}

