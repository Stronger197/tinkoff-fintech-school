package com.junior.stronger197.currencyexchanger.fragments.history.model

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.History
import com.junior.stronger197.currencyexchanger.fragments.filters.model.DateRange
import java.text.SimpleDateFormat
import java.util.*


class HistoryAdapter(var dataset: ArrayList<History>) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {

    private var mCleanCopyDataset = dataset

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.history_recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    fun updateDataset(dataset: ArrayList<History>) {
        Log.e("FILTER", "Update dataset invoke" +
                "()")
        this.dataset = dataset
        this.mCleanCopyDataset = dataset
        notifyDataSetChanged()
    }

    fun filter(dateRange: DateRange) {
        Log.e("FILTER", "FILTER INVOKE()")
        Log.e("FILTER", "clean copy dataset is: $mCleanCopyDataset")
        Log.e("FILTER", "dataset is: $dataset")
        dataset = ArrayList<History>()
        if (dateRange == null || dateRange.dateFrom == null) {
            Log.e("FILTER", "IS NULL")
            dataset.addAll(mCleanCopyDataset)
        } else {
            Log.e("FILTER", "FILTER NOT NULL")
            for (item in mCleanCopyDataset) {
                Log.e("FILTER", "ITEM: $item")
                if (dateRange.fateTo == null) {
                    if (item.date >= dateRange.dateFrom!!) {
                        dataset.add(item)
                    }
                } else {
                    if (item.date >= dateRange.dateFrom!! && item.date <= dateRange.fateTo!!) {
                        dataset.add(item)
                    }
                }
            }
        }

        Log.e("FILTER", "New dataset is: $dataset")

        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.firstName.text = dataset[position].firstCurrency
        holder.secName.text = dataset[position].secondaryCurrency
        holder.firstVal.text = dataset[position].firstValue.toString()
        holder.secVal.text = dataset[position].secondaryValue.toString()
        holder.date.text = SimpleDateFormat("dd-MM-yyyy").format(dataset[position].date * 1000)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var firstName: TextView = itemView.findViewById(R.id.textview_history_firstcurrencyname)
        var secName: TextView = itemView.findViewById(R.id.textview_history_secondarycurrencyname)
        var firstVal: TextView = itemView.findViewById(R.id.textview_history_firstvalue)
        var secVal: TextView = itemView.findViewById(R.id.textview_history_secondaryvalue)
        var date: TextView = itemView.findViewById(R.id.textview_history_date)
    }
}

