package com.junior.stronger197.currencyexchanger.helpers.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.History
import io.reactivex.Flowable

@Dao
interface HistoryDao {
    @Query("SELECT * from history")
    fun getAll(): List<History>

    @Query("SELECT * from history")
    fun getAllAsync(): Flowable<List<History>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(history: History)

    @Query("DELETE from history")
    fun deleteAll()
}