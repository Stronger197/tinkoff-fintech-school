package com.junior.stronger197.currencyexchanger.helpers.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface FixerApi {
    @GET("latest")
    fun getLatestRates(): Observable<FixerLatestResponse>
}

interface FixerOldApi {
    @GET("latest?base=EUR")
    fun getLatestRateByBase(@Query("symbols") toValue : Array<String>): Observable<FixerLatestByBaseResponse>
}