package com.junior.stronger197.currencyexchanger.fragments.exchangelist.model

import android.util.Log
import com.junior.stronger197.currencyexchanger.helpers.database.DatabaseHelper
import io.reactivex.subjects.PublishSubject


object ExchangeListModel {

    private val TAG = this.javaClass.name

    var currenciesList = PublishSubject.create<List<Currency>>()
//
//    fun updateCurrenciesList(data: List<Currency>) {
//        Log.e(TAG, "updateCurrenciesList() invoke with data: $data")
//        DatabaseHelper.upsertCurrencies(data)
//        currenciesList.onNext(DatabaseHelper.getCurrencies())
//        Log.e(TAG, "updateCurrenciesList() data after update: $currenciesList")
//    }
//
//    fun getCurrenciesList(): List<Currency> {
//        Log.e(TAG, "getCurrenciesList() return values is: ${DatabaseHelper.getCurrencies()}")
//        return DatabaseHelper.getCurrencies()
//    }
}