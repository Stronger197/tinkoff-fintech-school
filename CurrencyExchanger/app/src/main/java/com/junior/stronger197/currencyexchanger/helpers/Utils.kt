package com.junior.stronger197.currencyexchanger.helpers

import android.util.Log
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

val FIXER_API_KEY = "7b1143db68695af008c61debe5f8801f"
val FIXER_BASE_URL = "http://data.fixer.io/"

val FIXER_OLD_BASE_URL = "http://api.fixer.io/"

// http://data.fixer.io/lasest?access_key=7b1143db68695af008c61debe5f8801f

fun getCountryFlagFromLocale(countryCode: String): String {
    val flagOffset = 0x1F1E6
    val asciiOffset = 0x41

    val firstChar = Character.codePointAt(countryCode, 0) - asciiOffset + flagOffset
    val secondChar = Character.codePointAt(countryCode, 1) - asciiOffset + flagOffset

    return String(Character.toChars(firstChar)) + String(Character.toChars(secondChar))
}

fun parseDate(dateString : String) : Long {
    val formatter: DateFormat
    val date: Date
    formatter = SimpleDateFormat("yyyy-MM-dd")
    date = formatter.parse(dateString) as Date
    Log.e("Update date", "Date is: ${date.time / 1000}")

    return date.time / 1000
}