package com.junior.stronger197.currencyexchanger.helpers.database

import android.util.Log
import com.junior.stronger197.currencyexchanger.App
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import android.arch.persistence.room.Room


object DatabaseHelper {
    var db: AppDatabase = Room.databaseBuilder(App.getContext(), AppDatabase::class.java, "populus-database").build()
}