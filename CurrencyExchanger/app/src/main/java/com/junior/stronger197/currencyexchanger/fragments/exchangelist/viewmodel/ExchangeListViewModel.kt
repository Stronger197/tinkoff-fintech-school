package com.junior.stronger197.currencyexchanger.fragments.exchangelist.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import com.junior.stronger197.currencyexchanger.helpers.database.DatabaseHelper
import com.junior.stronger197.currencyexchanger.helpers.network.RetrofitHelper
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList


class ExchangeListViewModel : ViewModel() {

    private val TAG = this.javaClass.name

    private val mCompositeDisposable = CompositeDisposable()

    val loadingState: MutableLiveData<Boolean> by lazy {
        val data = MutableLiveData<Boolean>()
        data.value = true
        data
    }

    val currenciesList: MutableLiveData<ArrayList<Currency>> by lazy {
        val data = MutableLiveData<ArrayList<Currency>>()
        data.value = arrayListOf()
        data
    }

    init {
        // on first attach
        loadRates()
        observe()

        DatabaseHelper.db.currencyDao.getAllAsync()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Log.e("ROOM", "initMethod RECIVED data")
                    currenciesList.postValue(ArrayList(it))
                }
    }

    private fun observe() {
//        ExchangeListModel.currenciesList
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(getSecondObserver())
    }

    private fun getSecondObserver(): Observer<ArrayList<Currency>> {
        return object : Observer<ArrayList<Currency>> {
            override fun onNext(t: ArrayList<Currency>) {
                currenciesList.value = t
            }

            override fun onSubscribe(d: Disposable) {
            }


            override fun onError(e: Throwable) {
            }

            override fun onComplete() {
            }
        }
    }

    private fun loadRates() {
        Log.e(TAG, "loadRates() invoke")
        mCompositeDisposable.add(getLoadRatesObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { toListLoadingState() }
                .subscribe({
                    if (it.isNotEmpty()) {

                        Thread(Runnable {
                            it.forEach { DatabaseHelper.db.currencyDao.insert(it)
                            DatabaseHelper.db.currencyDao.update(it.name, it.rate!!, System.currentTimeMillis() / 1000)}
                        }).start()

//                        currenciesList.postValue(ArrayList(it))
                        toListLoadedState()
                    }
                }, {
                    toListLoadedState()
                }, {
                    Log.e("EMITTER HANDLER", "onComplete")
                    toListLoadedState()
                }))
    }

    private fun toListLoadingState() {
        loadingState.value = true
    }

    private fun toListLoadedState() {
        loadingState.value = false
    }


    private fun getLoadRatesObservable(): Observable<ArrayList<Currency>> {
        return Observable.create { emitter ->
          //  emitter.onNext(ArrayList(DatabaseHelper.db.currencyDao.getAllAvailableCurrenciesSync()))
            RetrofitHelper.getFixerApi().getLatestRates()
                    .subscribeOn(Schedulers.io())
                    .map { t ->
                var lastUpdate = System.currentTimeMillis() / 1000
                val data = ArrayList<Currency>()
                t.rates?.forEach { data.add(Currency(it.key, it.value, lastUpdate)) }
                data
            }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        Log.e("EMITTER", "ON NEXT: $it")
                        emitter.onNext(it)
                        emitter.onComplete()
                    }, {
                        emitter.onError(it)
                    })
        }
    }

    fun onStarClicked(currency: Currency) {
        Thread(Runnable {
//            val returnedValue = DatabaseHelper.upsert(currency.copy(name = currency.name, rate = currency.rate, lastUpdate = currency.lastUpdate, lastUsage = currency.lastUsage, isFavorite = !currency.isFavorite))
//            Log.e("ROOM", "ADDED: $currency")
//            Log.e("ROOM", "ADDED RETURN: $returnedValue")
            DatabaseHelper.db.currencyDao.update(currency.name, !currency.isFavorite)
        }).start()

    }

    private fun handleError(error: Throwable) {
        toListLoadedState()
    }

    private fun clearCompositeDisposable() {
        mCompositeDisposable.clear()
    }
}