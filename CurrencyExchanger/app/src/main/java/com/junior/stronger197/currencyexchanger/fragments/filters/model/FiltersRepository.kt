package com.junior.stronger197.currencyexchanger.fragments.filters.model

import io.reactivex.subjects.PublishSubject

object FiltersRepository {

    var dateRange : DateRange? = null

    var dateSubject  = PublishSubject.create<DateRange>()

    fun setDate(dateRange: DateRange) {
        this.dateRange = dateRange
        dateSubject.onNext(dateRange)
    }

}

data class DateRange(
        var dateFrom : Long? =null,
        var fateTo : Long? = null
)