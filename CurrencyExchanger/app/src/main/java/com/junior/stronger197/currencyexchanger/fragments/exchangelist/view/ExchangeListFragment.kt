package com.junior.stronger197.currencyexchanger.fragments.exchangelist.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.activities.base.view.BaseActivity
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.viewmodel.ExchangeListViewModel
import com.junior.stronger197.currencyexchanger.helpers.database.DatabaseHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_exchange_list.*
import java.util.ArrayList



class ExchangeListFragment : Fragment() {

    private val TAG = this.javaClass.name

    private lateinit var exchangeListViewModel: ExchangeListViewModel

    private lateinit var currenciesRecyclerView: RecyclerView



    val adapter = CurrenciesListAdapter(arrayListOf(), object : OnCurrencyClick {
        override fun onClick(currency: Currency, secondary: Currency?) {
           showExchangeFragment(currency, secondary)
        }

        override fun onLongClick(currency: Currency) {
            // TODO SOMETHING
        }

        override fun onStarClick(currency: Currency) {

            exchangeListViewModel.onStarClicked(currency)
        }
    })

    private fun showExchangeFragment(firstCurrency: Currency?, secondCurrency: Currency?) {
        (activity as BaseActivity).showExchangeFragment(firstCurrency, secondCurrency)
    }

    private fun showExchangeFragment(firstCurrency: Currency?) {
        showExchangeFragment(firstCurrency, null)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        exchangeListViewModel = ViewModelProviders.of(this).get(ExchangeListViewModel::class.java) // linking with view model
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_exchange_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        currenciesRecyclerView = activity!!.findViewById(R.id.recylcerview_exchangelist_currencies)
        currenciesRecyclerView.layoutManager = LinearLayoutManager(activity)
        currenciesRecyclerView.itemAnimator = DefaultItemAnimator()
        currenciesRecyclerView.adapter = adapter

        val callback = SimpleItemTouchHelperCallback(adapter)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(currenciesRecyclerView)

        observe()
    }

    private fun observe() {
        exchangeListViewModel.loadingState.observe(this, Observer<Boolean> {
            if (it!!) {
                progressbar_exchangelist_loading.visibility = View.VISIBLE
            } else {
                progressbar_exchangelist_loading.visibility = View.GONE
            }
        })

        exchangeListViewModel.currenciesList.observe(this, Observer<ArrayList<Currency>> {
            if(it != null) {
                Log.e("STARCHECK", "New data recived: " + it)
                adapter.updateDataset(it)
            }
        })
    }

    companion object {
        fun newInstance() = ExchangeListFragment()
    }
}
