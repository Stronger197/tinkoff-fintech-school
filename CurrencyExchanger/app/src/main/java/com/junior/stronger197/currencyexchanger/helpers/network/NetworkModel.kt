package com.junior.stronger197.currencyexchanger.helpers.network

import com.google.gson.annotations.SerializedName




data class FixerLatestResponse(
        @SerializedName("success") var success: Boolean? = null,
        @SerializedName("timestamp") var timestamp: Long? = null,
        @SerializedName("base") var base: String? = null,
        @SerializedName("date") var date: String? = null,
        @SerializedName("rates") var rates: Map<String, Float>? = null
)

data class FixerLatestByBaseResponse(
        @SerializedName("base")  var base: String? = null,
        @SerializedName("date")  var date: String? = null,
        @SerializedName("rates") var rates: Map<String, Float>? = null
)