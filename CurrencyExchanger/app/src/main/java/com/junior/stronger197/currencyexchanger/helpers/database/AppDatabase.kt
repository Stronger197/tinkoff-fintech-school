package com.junior.stronger197.currencyexchanger.helpers.database

import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Database
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.History


@Database(entities = arrayOf(Currency::class, History::class /*, AnotherEntityType.class, AThirdEntityType.class */), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val currencyDao: CurrencyDao

    abstract val historyDao: HistoryDao
}