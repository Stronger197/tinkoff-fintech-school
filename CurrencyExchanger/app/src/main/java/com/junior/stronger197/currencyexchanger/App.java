package com.junior.stronger197.currencyexchanger;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    public static App instanse;

    public static Context getContext() {
        return instanse.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instanse = this;
    }

}
