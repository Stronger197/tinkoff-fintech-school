package com.junior.stronger197.currencyexchanger.fragments.history.view

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.junior.stronger197.currencyexchanger.R
import com.junior.stronger197.currencyexchanger.activities.base.view.BaseActivity
import com.junior.stronger197.currencyexchanger.fragments.filters.model.FiltersRepository
import com.junior.stronger197.currencyexchanger.fragments.history.model.HistoryAdapter
import com.junior.stronger197.currencyexchanger.helpers.database.DatabaseHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_history.*

class HistoryFragment : Fragment() {

    private lateinit var currenciesRecyclerView: RecyclerView

    val adapter = HistoryAdapter(arrayListOf())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        currenciesRecyclerView = activity!!.findViewById(R.id.history_recycler_view)
        currenciesRecyclerView.layoutManager = LinearLayoutManager(activity)
        currenciesRecyclerView.itemAnimator = DefaultItemAnimator()
        currenciesRecyclerView.adapter = adapter

        open_filters_button.setOnClickListener {
            (activity as BaseActivity).showFiltersFragment()
        }

        Thread({
            val arrayList = ArrayList(DatabaseHelper.db.historyDao.getAll())
            activity!!.runOnUiThread {
                adapter.updateDataset(arrayList)
                if(FiltersRepository.dateRange != null) {
                    adapter.filter(FiltersRepository.dateRange!!)
                }
            }
        }).start()

        FiltersRepository.dateSubject.observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    adapter.filter(it)
                })

        super.onViewCreated(view, savedInstanceState)
    }


}