package com.junior.stronger197.currencyexchanger.helpers.network

import com.junior.stronger197.currencyexchanger.helpers.FIXER_API_KEY
import com.junior.stronger197.currencyexchanger.helpers.FIXER_BASE_URL
import com.junior.stronger197.currencyexchanger.helpers.FIXER_OLD_BASE_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.logging.HttpLoggingInterceptor




object RetrofitHelper {
    fun getFixerApi(): FixerApi {

        var retrofit = Retrofit.Builder()
                .baseUrl(FIXER_BASE_URL)
                .client(getKeyInterceptorOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(FixerApi::class.java)
    }

    fun getOldFixerApi(): FixerOldApi {
        var retrofit = Retrofit.Builder()
                .baseUrl(FIXER_OLD_BASE_URL)
                .client(getDefaultOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        return retrofit.create(FixerOldApi::class.java)

    }

    private fun getKeyInterceptorOkHttpClient(): OkHttpClient {

        val CONNECT_TIMEOUT = 10L
        val READ_TIMEOUT = 30L
        val WRITE_TIMEOUT = 30L

        return OkHttpClient()
                .newBuilder()
                .addInterceptor(ApiKeyInterceptor())
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build()
    }

    private fun getDefaultOkHttpClient() : OkHttpClient {

        val CONNECT_TIMEOUT = 10L
        val READ_TIMEOUT = 30L
        val WRITE_TIMEOUT = 30L

        val logging = HttpLoggingInterceptor()
// set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient()
                .newBuilder()
                .addInterceptor(logging)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .build()
    }

}


class ApiKeyInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        val url = request.url()
                .newBuilder()
                .addQueryParameter("access_key", FIXER_API_KEY)
                .build()

        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}