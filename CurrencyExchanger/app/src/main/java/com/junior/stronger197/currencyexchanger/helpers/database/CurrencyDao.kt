package com.junior.stronger197.currencyexchanger.helpers.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.Query
import com.junior.stronger197.currencyexchanger.fragments.exchangelist.model.Currency
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import io.reactivex.Flowable
import io.reactivex.Single


@Dao
interface CurrencyDao {
    @Query("SELECT * from currencies")
    fun getAll(): List<Currency>

    @Query("SELECT * from currencies")
    fun getAllAsync(): Flowable<List<Currency>>

    @Query("SELECT * from currencies where currency_name = :name")
    fun getAsync(name : String) : Flowable<Currency>

    @Insert(onConflict = IGNORE)
    fun insert(currency: Currency)

    @Query("UPDATE currencies SET currency_favorite = :isFavorite WHERE currency_name = :name;")
    fun update(name: String, isFavorite : Boolean)

    @Query("UPDATE currencies SET currency_rate = :rate, currency_last_update = :date WHERE currency_name = :name;")
    fun update(name: String, rate: Float, date : Long)

    @Query("DELETE from currencies")
    fun deleteAll()
}