package com.junior.stronger197.currencyexchanger.fragments.exchange.model

import io.reactivex.subjects.PublishSubject

object CurrenciesRepository {

    var firstCurrencyValue = PublishSubject.create<Double>()
    var secondCurrencyValue = PublishSubject.create<Double>()

    fun setFirstCurrencyValue(value : Double) {
        firstCurrencyValue.onNext(value)
    }

    fun setSecondCurrencyValue(value : Double) {
        secondCurrencyValue.onNext(value)
    }
}
