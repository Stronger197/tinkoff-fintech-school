import com.google.gson.Gson;
import network.ApiResponse.Pojo;
import network.internet.NetworkObserverInterface;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MainLogic implements NetworkObserverInterface.onAllCountries {

    private String from = "";
    private String to = "";
    private float value = 0F;

    public void runCurrencyConverter() {
        /** Первоначальный запрос названий валют и значений */
        Scanner in = new Scanner(System.in);
        showWelcomeDialog();
        System.out.println("Enter from currency");
        from = in.next().toUpperCase(); // Получение исходной валюты
        System.out.println("Enter to currency");
        to = in.next().toUpperCase(); // Получение итоговой валюты
        System.out.println();
        System.out.print(from + " => " + to + " : ");
        try {
            value = in.nextFloat(); // Получение исходного значения
        } catch (InputMismatchException e) {
            System.out.println("Исходное значение должно быть числом");
            return;
        }

        System.out.println();
        new DataUsage(this).requestAllCountries();
    }

    private void showWelcomeDialog() {
        System.out.println("This is a simple currency converter");
    }

    private float convertFrom(float value, float fromRate) {
        return value / fromRate;
    }

    private float convertTo(float value, float toRate) {
        return value * toRate;
    }



    @Override
    public void onResponse(Pojo.ApiResponse response) {
        System.out.println();
       // System.out.println("On response invoke " + new Gson().toJson(response));
        Pojo.ApiResponse apiResponse = response;

        if (apiResponse != null) {
            if ((apiResponse.getRates().containsKey(from) || from.equals("EUR")) && (apiResponse.getRates().containsKey(to) || to.equals("EUR"))) {
                float fromRate = from.equals("EUR") ? 1 : apiResponse.getRates().get(from);
                float toRate = to.equals("EUR") ? 1 : apiResponse.getRates().get(to);
                float valueInEUR = convertFrom(value, fromRate);
                System.out.println(convertTo(valueInEUR, toRate));
            } else {
                System.out.println("Введена неверная валюта");
            }
        } else {
            System.out.println("Ошибка получения данных с сервера");
        }
    }

    @Override
    public void onFailure(String errorMessage) {
        System.out.println();
        System.out.println(errorMessage);
    }

    @Override
    public void progressChanged() {
        System.out.print(".");
    }
}
