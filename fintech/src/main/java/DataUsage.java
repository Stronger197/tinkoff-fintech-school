import com.google.gson.Gson;
import network.ApiResponse.Pojo;
import network.database.DBUsage;
import network.internet.NetworkObserverInterface;
import network.internet.RetrofitModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataUsage extends Thread {
    private final NetworkObserverInterface.onAllCountries observer;

    DataUsage(NetworkObserverInterface.onAllCountries observer) {
        this.observer = observer;
    }

    public void requestAllCountries() {
        this.start();
    }


    public void run() {
        observer.progressChanged();
        String cachedDataJson = DBUsage.readStringFromFile();
        /** Если кэш не пуст получить значения, иначе отобразить ошибку и попробовать запросить данные с сервера */
        if (cachedDataJson != null) {
            observer.progressChanged();
            Pojo.CachedData cachedData = new Gson().fromJson(cachedDataJson, Pojo.CachedData.class);
            /** Если данные о валюте устарели (прошел день) запросить новые данные с сервера */
            if (DateHelper.getCurrentDate().getDayOfMonth() - cachedData.getDate().getDayOfMonth() >= 1) {
                //   System.out.println("Прошло более одного дня данные устарели");
                getDataFromNet(observer); // Запрос данных c сервера

            } else {
                observer.onResponse(cachedData.getData()); // Если, данные актуальные отобразить их из кэша

            }
        } else {
            observer.progressChanged();
            getDataFromNet(observer);
        }
    }


    /**
     * Запрос данных с сервера, в случае неудачи попытка получения данных из кэша
     */
    private void getDataFromNet(NetworkObserverInterface.onAllCountries observer) {
        observer.progressChanged();
        new RetrofitModel().getFixerApi("http://api.fixer.io/").getAllRates().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    observer.progressChanged();
                    // System.out.println(response.body().toString());
                    String responseJson = response.body().toString();
                    Pojo.ApiResponse apiResponse = new Gson().fromJson(responseJson, Pojo.ApiResponse.class);

                    /** сохранение нового значения в базу данных */
                    DBUsage.writeStringToFile(new Gson().toJson(new Pojo.CachedData(DateHelper.getCurrentDate(), apiResponse)));

                    observer.onResponse(apiResponse);
                    observer.progressChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                    observer.progressChanged();
                    observer.progressChanged();
                    observer.onFailure("Ошибка получения данных о текущем курсе\nПроверьте подключение к интернету");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                observer.progressChanged();
                observer.progressChanged();
                String cachedDataJson = DBUsage.readStringFromFile();
                if (cachedDataJson != null) {
                    Pojo.CachedData cachedData = new Gson().fromJson(cachedDataJson, Pojo.CachedData.class);
                    observer.onResponse(cachedData.getData());
                } else {
                    observer.onFailure("Ошибка получения данных о текущем курсе\nПроверьте подключение к интернету");
                }
            }
        });
    }
}
