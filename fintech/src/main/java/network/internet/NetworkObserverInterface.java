package network.internet;

import network.ApiResponse.Pojo;

public interface NetworkObserverInterface {
    interface onAllCountries{
        void onResponse(Pojo.ApiResponse response);
        void onFailure(String errorMessage);
        void progressChanged();
    }
}
