package network.internet;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitModel {

    private Retrofit retrofit = null;

    public RetrofitInterface.FixerApi getFixerApi(String baseUrl) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }

        return retrofit.create(RetrofitInterface.FixerApi.class);
    }


}
