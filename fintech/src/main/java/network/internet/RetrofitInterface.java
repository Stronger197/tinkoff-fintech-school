package network.internet;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

public interface RetrofitInterface {
    /** интерфейс для работы с API конверации валют */
    interface FixerApi {
        /** Получение рейтинга для одной страны (не используется) */
        @GET("latest")
        Call<String> getRates(@Query("base") String fromValue, @Query("symbols") String toValue);

        /** получение списка валют для всех стран */
        @GET("latest")
        @Streaming
        Call<String> getAllRates();
    }
}
