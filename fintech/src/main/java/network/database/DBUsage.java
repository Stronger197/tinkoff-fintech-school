package network.database;

import java.io.*;

public class DBUsage {
    public static boolean writeStringToFile(String text) {
        /** Создание нового файла */
        try {
        File f = new File("simpleDB.txt");
        f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /** запись в файл */
        BufferedWriter writer = null;
        try
        {
            writer = new BufferedWriter( new FileWriter("simpleDB.txt"));
            writer.write(text);
            return true;
        }
        catch ( IOException e)
        {
            // TODO обработать ошибку
            return false;
        }
        finally
        {
            try
            {
                if ( writer != null)
                    writer.close( );
            }
            catch ( IOException e)
            {
                // TODO handle exeption
            }
            return false;
        }
    }

    public static String readStringFromFile() {
        /** создание файла */
        try {
            File f = new File("simpleDB.txt");
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /** чтение из файла */
        try (BufferedReader br = new BufferedReader(new FileReader("simpleDB.txt"))) {
            String s = br.readLine();
            if(s != null) {
                return s;
            } else {
                return null;
            }
        } catch (IOException ex) {
            return null;
        }
    }
}
