package network.ApiResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;
import java.util.Map;

public class Pojo {

    /** объект для хранения данных в кэше.
     * Содержит ответ от сервера конвертации валют и дату получения данных
     */
    public static class CachedData{
        private LocalDate date;
        private ApiResponse data;

        public CachedData(LocalDate date, ApiResponse response) {
            this.date = date;
            data = response;
        }

        public void setDate(LocalDate date) {
            this.date = date;
        }

        public LocalDate getDate() {
            return date;
        }


        public void setData(ApiResponse data) {
             this.data = data;
        }

        public ApiResponse getData() {
            return data;
        }
    }

    /** ответ от сервера конвертации валют */
    public class ApiResponse {
        @SerializedName("base")
        @Expose
        private String base;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("rates")
        @Expose
        private Map<String, Float> rates;

        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Map<String, Float> getRates() {
            return rates;
        }

        public void setRates(Map<String, Float> rates) {
            this.rates = rates;
        }
    }
}
