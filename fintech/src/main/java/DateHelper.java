import java.time.LocalDate;

public class DateHelper {
    public static LocalDate getCurrentDate() {
        return LocalDate.now();
    }
}
